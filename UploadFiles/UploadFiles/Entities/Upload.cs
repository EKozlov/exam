﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UploadFiles.Entities
{
    public class Upload
    {
        public string Id { get; set; }
        public int SizeInBytes { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }
        public string Extension { get; set; }
        public string ContentType { get; set; }
        public string Type { get; set; }
        public DateTime CreatedDate { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }

        public int Bitrate { get; set; }
        public double Duration { get; set; }
        
    }
}
