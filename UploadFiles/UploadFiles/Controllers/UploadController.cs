﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UploadFiles.Entities;

namespace UploadFiles.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UploadController : ControllerBase
    {
        private readonly DAL.ApplicationDbContext _db;
        private readonly IHostingEnvironment _appEnvironment;
        public UploadController(DAL.ApplicationDbContext db, IHostingEnvironment hostingEnvironment)
        {
            _db = db;
            _appEnvironment = hostingEnvironment;
        }
        /// <summary>
        /// Загрузка файлов
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("")]

        public async Task<IActionResult> Create(IFormFile file, UploadModel model)
        {

            var size = file.Length;

            var stream = file.OpenReadStream();
            stream.Position = 0;

            var memoryStream = new MemoryStream();
            await stream.CopyToAsync(memoryStream);

            var bytes = memoryStream.ToArray();

            var uploadEntity = new Upload
            {
                Id = Guid.NewGuid().ToString(),
                Extension = Path.GetExtension(file.FileName),
                SizeInBytes = (int)size,
                ContentType = file.ContentType,
                Description = model.Description,
                Name = model.Name
            };

            _db.Uploads.Add(uploadEntity);
            await _db.SaveChangesAsync();

            return Ok();

        }

        [HttpGet("")]
        public async Task<IActionResult> GetFiles()
        {
            IQueryable<Upload> uploads;

            uploads = _db.Uploads.AsQueryable();

            return Ok(GetUploads(uploads).ToList());
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(string id)
        {
            var query = _db.Uploads.Where(x => x.Id == id).AsQueryable();

            return Ok(GetUploads(query).FirstOrDefault());
        }



        private IQueryable<Upload> GetUploads(IQueryable<Upload> query)
        {

            var result = query.Select(x => new Upload
            {
                SizeInBytes = x.SizeInBytes,
                ContentType = x.ContentType,
                Extension = x.Extension,
                Id = x.Id,
                Name = x.Name,
                Description = x.Description

            });

            return result;
        }



    }

    public class UploadModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }


}